import blogsController from './blogsList.controller.js';

const blogsListComponent = {
	templateUrl: './src/app/blogsList/blogsList.template.html',
	controller: blogsController,
	bindings: {
		blogs: '='
	}
}

export default blogsListComponent