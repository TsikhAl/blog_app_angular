import angular from 'angular';
import messages from 'angular-messages';
import route from 'angular-route';
import '../styles/main.scss';
import paginationComponent from './pagination/pagination.js';
import blogsListComponent from './blogsList/blogsList.js';
import addEditBlogComponent from './addEditBlog/addEditblog.js';
import addBlogBtnComponent from './addBlogBtn/addBlogBtn.js';
import addEditValidationDir from './directives/addEditValidation.directive.js';
import blogsFactory from './factories/blogsFactory.service.js';
import blogsAppController from './controllers/blogsAppController.controller.js';

var app = angular.module('blogsApp', [route, messages]);

app.config(function($routeProvider, $locationProvider) {
	$routeProvider
	.when('/blogs', {
		template: `
            <blogs-list blogs="$ctrl.blogs"></blogs-list>
			<add-blog-btn></add-blog-btn>
		`
	})
	.when('/blog/add', {
		template: '<add-edit-blog></add-edit-blog>'
	})
	.when('/blog/edit/:id', {
		template: '<add-edit-blog></add-edit-blog>'
	})
	.otherwise({redirectTo: '/blogs'})

	$locationProvider.html5Mode(true);
});

app.filter('range', function() {
	return function(val, range) {
		range = parseInt(range);
		for (var i = 0; i < range; i++)
			val.push(i);
		return val;
	};
});

app.directive('addEditValidation', addEditValidationDir);

app.factory('blogsFactory', blogsFactory);

app.controller('blogsAppController', blogsAppController);

app.component('blogsList', blogsListComponent);
app.component('pagination', paginationComponent);
app.component('addBlogBtn', addBlogBtnComponent);
app.component('addEditBlog', addEditBlogComponent);