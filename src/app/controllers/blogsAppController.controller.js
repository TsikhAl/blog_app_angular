function blogsAppController($scope, blogsFactory) {
    var ctrl = this;
	$scope.isLoading = true;
	blogsFactory.getBlogs()
		.then(function(res) {
            ctrl.blogs = res;
			$scope.isLoading = false;
		})
}

blogsAppController.$inject = ['$scope', 'blogsFactory'];

export default blogsAppController;