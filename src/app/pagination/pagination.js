import paginationController from './pagination.controller.js';

const paginationComponent = {
	templateUrl: './src/app/pagination/pagination.template.html',
	controller: paginationController,
	bindings: {
		blogs: '=',
		page: '='
	}
}

export default paginationComponent