import addEditBlogController from './addEditBlog.controller.js';

const addEditBlogComponent = {
	templateUrl: './src/app/addEditBlog/addEditBlog.template.html',
	controller: addEditBlogController
}

export default addEditBlogComponent