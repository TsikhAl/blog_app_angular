function addEditBlogController($scope, blogsFactory, $routeParams, $location) {
	var ctrl = this;

	ctrl.submitted = true;
	ctrl.changingBlogId = $routeParams.id || '';

	if (ctrl.changingBlogId) {
		blogsFactory.getBlogs()
		.then(function(res) {
			var blog = res.find(elem => elem._id === ctrl.changingBlogId);
			ctrl.newBlogTitle = blog.title;
			ctrl.newBlogAuthor = blog.author;
            ctrl.newBlogDescription = blog.description;
            ctrl.newBlogDate = blog.date
		})
	}

	ctrl.getNewBlog = function() {
		var newBlog = {
			title: ctrl.newBlogTitle,
			author: ctrl.newBlogAuthor,
            description: ctrl.newBlogDescription,
            date: ctrl.newBlogDate
		};

		ctrl.submitted = true;
		ctrl.newBlogTitle = '';
		ctrl.newBlogAuthor = '';
        ctrl.newBlogDescription = '';
        ctrl.newBlogDate = '';

		return newBlog;
	}
	
	ctrl.addBlog = function () {
		var newBlog = ctrl.getNewBlog();

		if (!newBlog.title || !newBlog.author || !newBlog.description || !newBlog.date) {
			ctrl.submitted = false;
			return;
		};

		blogsFactory.addBlog(newBlog);
		$location.url('/');
	};

	ctrl.editTodo = function() {
		var newBlog = ctrl.getNewBlog();

		if (!newBlog.title || !newBlog.author || !newBlog.description || !newBlog.date) {
			ctrl.submitted = false;
			return;
		};

		blogsFactory.changeBlog(ctrl.changingBlogId, newBlog);
		$location.url('/');
	}
	
}

addEditBlogController.$inject = ['$scope', 'blogsFactory', '$routeParams', '$location'];

export default addEditBlogController;