import addBlogBtnController from './addBlogBtn.controller.js';

const addBlogBtnComponent = {
	templateUrl: './src/app/addBlogBtn/addBlogBtn.template.html',
	controller: addBlogBtnController
}

export default addBlogBtnComponent